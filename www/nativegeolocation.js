var NativeGeolocation = {
	debugStatus: false,
    watchPosition: function (success, failure, options) {

		if(NativeGeolocation.debugStatus) console.log("watchPosition", options);

    	NativeGeolocation.nativeStartLocation(options);

	    NativeGeolocation.nativeGetLocation(success);

	    return setInterval(function(){ 

	    	NativeGeolocation.nativeGetLocation(success); 

	    },options.timeout);
    },
    clearWatch: function (watchInterval) {

		if (NativeGeolocation.debugStatus) console.log("clearWatch");

    	try{ clearInterval(watchInterval); }catch(err){} // trying to put this first 
        
        NativeGeolocation.nativeStopLocation();
    },
    getCurrentPosition: function (success, failure, options) {

		if (NativeGeolocation.debugStatus) console.log("getCurrentPosition");

    	NativeGeolocation.nativeGetLocation(success);
    },
    nativeStartLocation: function(options){

		if (NativeGeolocation.debugStatus) console.log("nativeStartLocation");

    	cordova.exec(function(){}, function(){}, "NativeGeolocation", "nativeStartLocation", [options]);
    },
    nativeStopLocation: function(){

		if (NativeGeolocation.debugStatus) console.log("nativeStopLocation");

    	cordova.exec(function(){}, function(){}, "NativeGeolocation", "nativeStopLocation", []);
    },
    nativeGetLocation: function(success){

		if (NativeGeolocation.debugStatus) console.log("nativeGetLocation");

    	cordova.exec(
	    function(data){

			success({
	            coords:{ 
					latitude	: data.latitude,
					longitude	: data.longitude,
					accuracy	: data.accuracy,
					heading		: data.heading,
					speed		: data.speed
	            }
	        });
	    },
	    function(){}, "NativeGeolocation", "nativeGetLocation", []);
    }
};

module.exports = NativeGeolocation;
