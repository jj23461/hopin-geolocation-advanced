package com.hopin.cordova.nativegeolocation;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaInterface;

import android.os.Bundle;
import android.content.Context;
import org.apache.cordova.CallbackContext;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import java.util.Date;

import android.location.Location;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.LocationServices;
import android.location.LocationManager;
import android.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import android.os.Looper;
import android.content.pm.PackageManager;
import android.os.Build;
import android.Manifest;

import android.widget.Toast;
import android.util.Log;

public class NativeGeolocation extends CordovaPlugin {

    // variables for location
    private static boolean isDebugEnabled        = false;

    private static boolean isGpsEnabled          = false;
    private static boolean isNetworkEnabled      = false;
    private static boolean isPassiveEnabled      = false;
    
    private static Location locGpsNew            = null;
    private static Date locGpsNewTime            = null;
    private static Location locGpsLast           = null;
    
    private static Location locNetNew            = null;
    private static Date locNetNewTime            = null;
    private static Location locNetLast           = null;
    
    private static Location locPasNew            = null;
    private static Date locPasNewTime            = null;
    private static Location locPasLast           = null;
    
    private static LocationManager locManagerHandler;
    private static GoogleApiClient locClientHandler;
    private static LocationRequest locationRequest;
    public static Integer LocationUpdateInterval = 1000;
    
    public static String geolocationMethod       = null;

    public static Context activity;
    private CallbackContext mCbContext;
    private static final int LOCATION_PERMISSION_REQUEST = 0;


    private String [] permissions = { Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION };

    @Override
    public void requestPermissions(int requestCode) {
        cordova.requestPermissions(this, requestCode, permissions);
    }

    @Override
    public boolean hasPermisssion() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        for (String p : permissions) {
            if (PackageManager.PERMISSION_DENIED == cordova.getActivity().checkSelfPermission(p)) {
                return false;
            }
        }

        return true;
    }


    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        activity = this.cordova.getActivity().getApplication();
    }

    public static Location GiveMeBestLocation() {
        
        if(isDebugEnabled){ Log.d("NativeGeolocation", "GiveMeBestLocation"); }
        if(isDebugEnabled){ showToast("GiveMeBestLocation"); }

        Location locBest = null;

        try {
            // The best option
            if (locGpsNew != null && locGpsNewTime != null && locGpsNewTime.getTime() > new Date().getTime() - 180000 && locNetNew != null && locGpsNew.getAccuracy() < locNetNew.getAccuracy()) {
                // if gpsNew available, and it is newer than 3 minutes, and netNew is available, and gpsNew Acc is lower than netNew Acc, this is a LG fix that increments slowly GPS accuracy
                locBest = locGpsNew;
                if(isDebugEnabled){ showToast("GiveMeBestLocation locGpsNew"); }
            } else if (locGpsNew != null && locNetNew == null) {
                // if gpsNew available but its older than 3mins, and netNew is NOT available, id doesnt matter how old the gpsNew is
                locBest = locGpsNew;
                if(isDebugEnabled){ showToast("GiveMeBestLocation locGpsNew"); }
            } else if (locGpsNew != null && locNetNew != null && locGpsNewTime != null && locNetNewTime != null && locGpsNewTime.getTime() - locNetNewTime.getTime() > 15000 && locGpsNew.getAccuracy() < locNetNew.getAccuracy()) {
                // if gpsNew available but its older than 3mins, and netNew is available, and gpsNew is newer than newNew, and gpsNew Acc is lower than netNew Acc, this is a LG fix that increments slowly GPS accuracy
                locBest = locGpsNew;
                if(isDebugEnabled){ showToast("GiveMeBestLocation locGpsNew"); }
            } else if (locGpsNew != null && locNetNew != null) {
                // if gpsNew available but its older than 3mins, and netNew is available, and gpsNew is newer than newNew
                locBest = locNetNew;
                if(isDebugEnabled){ showToast("GiveMeBestLocation locNetNew"); }
            } else if (locNetNew != null && locGpsLast != null && GetDistance(locNetNew.getLatitude(), locNetNew.getLongitude(), locGpsLast.getLatitude(), locGpsLast.getLongitude()) < locNetNew.getAccuracy()) {
                // if gpsLast available, if netNew available, and gps Last is inside of the netNew accuracy
                locBest = locGpsLast;
                locBest.setAccuracy(1000);
                if(isDebugEnabled){ showToast("GiveMeBestLocation locGpsLast"); }
            } else if (locNetNew != null) {
                // if netNew available
                locBest = locNetNew;
                if(isDebugEnabled){ showToast("GiveMeBestLocation locNetNew"); }
            } else if (locNetNew == null && locNetLast != null && locGpsLast != null && GetDistance(locNetLast.getLatitude(), locNetLast.getLongitude(), locGpsLast.getLatitude(), locGpsLast.getLongitude()) < locNetLast.getAccuracy()) {
                // if netNew NOT available, if netLast available, if gpsLast available, and gps Last is inside of the netLast accuracy
                locBest = locGpsLast;
                locBest.setAccuracy(1000);
                if(isDebugEnabled){ showToast("GiveMeBestLocation locGpsLast"); }
            } else if (locNetLast != null) {
                // if netLast available
                locBest = locNetLast;
                if(isDebugEnabled){ showToast("GiveMeBestLocation locNetLast"); }
            } else if (locPasNew != null) {
                // if passiveNew available
                locBest = locPasNew;
                if(isDebugEnabled){ showToast("GiveMeBestLocation locPasNew"); }
            } else if (locGpsLast != null) {
                // if locGpsLast available
                locBest = locGpsLast;
                locBest.setAccuracy(1000);
                if(isDebugEnabled){ showToast("GiveMeBestLocation locGpsLast"); }
            } else if (locPasLast != null) {
                // if passiveLast available
                locBest = locPasLast;
                if(isDebugEnabled){ showToast("GiveMeBestLocation locPasLast"); }
            }
        } 
        catch (NullPointerException e) {
            if(isDebugEnabled){ showToast("GiveMeBestLocation error"); }
        }

        if (locBest == null){
            return null;
        }

        // round current results
        double roundedLat = Math.rint(locBest.getLatitude() * 100000) / 100000;
        double roundedLng = Math.rint(locBest.getLongitude() * 100000) / 100000;

        locBest.setLatitude(roundedLat);
        locBest.setLongitude(roundedLng);

        return locBest;
    }

    private static double GetDistance(double lat1, double lng1, double lat2, double lng2) {
        int R = 6371; // km
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);
        lng1 = Math.toRadians(lng1);
        lng2 = Math.toRadians(lng2);

        double x = (lng2 - lng1) * Math.cos((lat1 + lat2) / 2);
        double y = (lat2 - lat1);
        return Math.sqrt(x * x + y * y) * R * 1000;// in meters
    }

    public static void StartLocation(String type){

        // we can not do this, because location client returns only changes
        // locGpsNew     = null;
        // locGpsNewTime = null;
        // locGpsLast    = null;
        // locNetNew     = null;
        // locNetLast    = null;
        // locPasLast    = null;
        // locPasNew     = null;

        if(geolocationMethod != null){
            // stop first if already started
            StopLocation();
        }

        try{
            Looper.prepare();
        }
        catch(Exception er){}

        if(Looper.getMainLooper() == null){
            StartLocation(type);
            return;
        }

        if (type == null){
            // try this first
            type = "locationClient";
        }

        if(type.equals("locationClient")) {

            if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity) == ConnectionResult.SUCCESS) {

                if(isDebugEnabled){ Log.d("NativeGeolocation", "StartLocation locationClient"); }
                if(isDebugEnabled){ showToast("StartLocation locationClient"); }

                locClientHandler = new GoogleApiClient.Builder(activity)
                        .addConnectionCallbacks(locClientCallback)
                        .addOnConnectionFailedListener(locClientFailedListener)
                        .addApi(LocationServices.API)
                        .build();

                locClientHandler.connect();
                geolocationMethod = "locationClient";
            } else {
                StartLocation("locationManager");
                return;
            }
        }

        if (type.equals("locationManager")) {

            if(isDebugEnabled){ Log.d("NativeGeolocation", "StartLocation locationManager"); }
            if(isDebugEnabled){ showToast("StartLocation locationManager"); }

            locManagerHandler = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

            geolocationMethod = "locationManager";

            // Get Last Positions
            try {
                locGpsLast = locManagerHandler.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            } 
            catch (Exception e) {
                locGpsLast = null;
            }

            try {
                locNetLast = locManagerHandler.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            } 
            catch (Exception e) {
                locNetLast = null;
            }

            try {
                locPasLast = locManagerHandler.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            } 
            catch (Exception e) {
                locPasLast = null;
            }

            // Check whats available
            try {
                isGpsEnabled = locManagerHandler.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } 
            catch (Exception ex) {
            }

            try {
                isNetworkEnabled = locManagerHandler.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            } 
            catch (Exception ex) {
            }

            try {
                isPassiveEnabled = locManagerHandler.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
            } 
            catch (Exception ex) {
            }

            // Bind listeners
            if (isGpsEnabled) {
                locManagerHandler.requestLocationUpdates(LocationManager.GPS_PROVIDER, LocationUpdateInterval, 0, locManagerListenerGps, Looper.getMainLooper());
            }

            if (isNetworkEnabled) {
                locManagerHandler.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 0, locManagerListenerNetwork, Looper.getMainLooper());
            }

            // if (isPassiveEnabled) {
            //     locManagerHandler.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 15000, 0, locManagerListenerPassive, Looper.getMainLooper());
            // }
        }
    }


    public static com.google.android.gms.location.LocationListener locClientListener = new com.google.android.gms.location.LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            locGpsNew = location;
            locGpsNewTime = new Date();
            if(isDebugEnabled){ Log.d("NativeGeolocation", "locClientListener onLocationChanged"); }
        }
    };

    private static LocationListener locManagerListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
            locGpsNew     = location;
            locGpsNewTime = new Date();
            if(isDebugEnabled){ Log.d("NativeGeolocation", "locManagerListenerGps onLocationChanged"); }
        }
        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    private static LocationListener locManagerListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            locNetNew     = location;
            locNetNewTime = new Date();
            if(isDebugEnabled){ Log.d("NativeGeolocation", "locManagerListenerNetwork onLocationChanged"); }
        }
        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    // private static LocationListener locManagerListenerPassive = new LocationListener() {
    //     public void onLocationChanged(Location location) {
    //         locPasNew = location;
    //         locPasNewTime = new Date();
    //     }
    //     public void onProviderDisabled(String provider) {}
    //     public void onProviderEnabled(String provider) {}
    //     public void onStatusChanged(String provider, int status, Bundle extras) {}
    // };

    final static GoogleApiClient.ConnectionCallbacks locClientCallback = new GoogleApiClient.ConnectionCallbacks() {
        @Override
        public void onConnected(Bundle bundle) {
            if(isDebugEnabled){ Log.d("NativeGeolocation", "locClientCallback onConnected"); }
            
            locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(LocationUpdateInterval);

            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(locClientHandler, locationRequest, locClientListener);
            } catch (Exception e) {
                if(isDebugEnabled){ Log.d("*** loc", "handler fail"); }
            }
        }

        @Override
        public void onConnectionSuspended(int cause) {
            if(isDebugEnabled){ Log.d("NativeGeolocation", "locClientCallback onConnectionSuspended"); }
        }
    };

    final static GoogleApiClient.OnConnectionFailedListener locClientFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            if(isDebugEnabled){ Log.d("NativeGeolocation", "locClientFailedListener onConnectionFailed"); }
            StartLocation("locationManager");
        }
    };
    
    public static void StopLocation() {

        if(isDebugEnabled){ Log.d("NativeGeolocation", "StopLocation"); }
        if(isDebugEnabled){ showToast("StopLocation"); }

        if (geolocationMethod == null) {
            // already stopped
            return;
        }

        if (geolocationMethod.equals("locationManager")) {
            try {
                locManagerHandler.removeUpdates(locManagerListenerGps);
                locManagerHandler.removeUpdates(locManagerListenerNetwork);
            } catch (Exception e) {}
        }
        else if (geolocationMethod.equals("locationClient")) {
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(locClientHandler, locClientListener);
                locClientHandler.disconnect();
            } catch (Exception e) {}
        }
        geolocationMethod = null; // clear the type
    }


    CallbackContext context;

    public boolean execute(final String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if(isDebugEnabled){ Log.d("NativeGeolocation", "execute "+action); }
        if(isDebugEnabled){ showToast(action); }

        context = callbackContext;
        if ("nativeStartLocation".equals(action)) {

            LocationUpdateInterval = Integer.parseInt( new JSONObject(args.getString(0)).getString("timeout") );
            StartLocation(geolocationMethod);
            context.success();
            return true;
        }
        else if ("nativeStopLocation".equals(action)) {

            StopLocation();
            context.success();
            return true;
        }
        else if ("nativeGetLocation".equals(action)) {

            if (hasPermisssion() == false) {
                mCbContext = callbackContext;
                requestPermissions(LOCATION_PERMISSION_REQUEST);
                return true;
            }

            final Location currBestLoc  = GiveMeBestLocation();
            final JSONObject jsonResult = new JSONObject();

            if(isDebugEnabled){ showToast("currBestLoc:"+currBestLoc); }

            // default
            jsonResult.put("latitude", 0)
                        .put("longitude", 0)
                        .put("accuracy", 0)
                        .put("heading", 0)
                        .put("speed", 0);

            try {
                jsonResult.put("latitude", currBestLoc.getLatitude())
                            .put("longitude", currBestLoc.getLongitude())
                            .put("accuracy", currBestLoc.getAccuracy())
                            .put("heading", currBestLoc.getBearing())
                            .put("speed", currBestLoc.getSpeed());
                    
            } catch (JSONException e) {
            } catch (NullPointerException e) {
            }

            context.success(jsonResult);
            return true;
        }

        context.success();
        return false;
    }
       

    public static void showToast(String toLog){

        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(activity, toLog, duration);
        toast.show();
    }
}

    